<?php
    namespace lab3\Fastfood;

    require_once "Fastfood/Personal.php";
    use lab3\Fastfood\Personal;

    class Cook extends Personal
    { 
        public $free;

        function __construct($name, $free)
        {
            $this->name = $name;
            $this->free = $free;
        }

        public function acceptOrder(array $order, $cost, $cashier, $customer)
        {
            echo "Повар ".$this->name.": Заказ принят.<br>";
            foreach($order as $food){
                $this->cooked($food);
            }
            echo "Повар ".$this->name.": Заказ готов.<br>";
            $cashier->giveOrder($cost, $customer);
        }

        private function cooked($food){
            echo "Повар ".$this->name.": ".$food->name." готово.<br>";
        }
    }

?>