<?php
    namespace lab3\Fastfood;

    class Food
    { 
        public $name;
        public $cost;

        function __construct($name, $cost)
        {
            $this->name = $name;
            $this->cost = $cost;
        }
    }

?>