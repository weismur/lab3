<?php
    namespace lab3\Fastfood;

    class Customer
    { 
        public $name;

        function __construct($name)
        {
            $this->name = $name;
        }

        public function giveOrder(array $order, $cost , Cashier $cashier)
        {
            echo"Покупатель ".$this->name.": Я хочу";
            foreach ($order as $food){
                echo " ".$food->name;
            }
            echo".<br>";
            $cashier->acceptOrder($order, $cost, $this);
        }
        public function acceptOrder()
        {
            echo"Покупатель ".$this->name.": Спасибо.<br>";
        }
    }

?>