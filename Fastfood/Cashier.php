<?php
    namespace lab3\Fastfood;

    require_once "Fastfood/Personal.php";
    use lab3\Fastfood\Personal;

    class Cashier extends Personal
    { 
        public $name;
        public $CookList;

        function __construct($name, $CookList)
        {
            $this->name = $name;
            $this->CookList = $CookList;
        }

        public function acceptOrder(array $order, $cost, $customer)
        {
            $totalCost = 0;
            foreach($order as $food){
                $totalCost += $food->cost;
            }
            if($totalCost > $cost){
                echo "Кассир ".$this->name.": Для оплаты заказа нехватает ".$totalCost-$cost.".";
            }else{
                $cook = $this->findCook();
                if($cook == null){
                    echo "Кассир ".$this->name.":Сейчас все повора заняты.";
                }else{
                    echo "Кассир ".$this->name.": Заказ принят и передан повару ".$cook->name.".<br>";
                    $cook -> acceptOrder($order, $cost-$totalCost, $this, $customer);
                }
            }
        }

        private function findCook(){
            foreach($this->CookList as $Cook){
                if($Cook->free){
                    return $Cook;
                }
            }
            return null;
        }

        public function giveOrder($cost, $customer){
            echo "Кассир ".$this->name.": Вот ваш заказ";
            if($cost != 0){
                echo " и сдача ".$cost;
            }
            echo ".<br>";
            $customer->acceptOrder();
        }
    }

?>