<?php
    namespace lab3\Transports;

    require_once "interfaces/Interface.php";
    use interfaces\transport\canDeliver;
    require_once "Transports/Package.php";
    use lab3\Transports\Package;

    class Car extends Transport implements canDeliver
    { 
        public function deliver(Package $gruz)
        {
            if ($this->maxWeight <= $gruz->weight){
                return "Груз ".$gruz->name." слишком тяжелый для машины ". $this->name;
            }
            if ($this->maxVolume <= $gruz->volume){
                return "Груз ".$gruz->name." слишком большой для машины ". $this->name;
            }
            return "Машина ".$this->name." "." перевёзла груз "." ".$gruz->name;
        }
    }

?>