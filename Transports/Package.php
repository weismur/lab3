<?php 
    namespace lab3\Transports;
    
    class Package 
    {
        public $name;
        public $weight;
        public $volume;

        function __construct($name, $weight, $volume)
        {
            $this->name = $name;
            $this->weight = $weight;
            $this->volume = $volume;
        }
    }

    
?>