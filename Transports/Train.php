<?php
    namespace lab3\Transports;

    require_once "interfaces/Interface.php";
    use interfaces\transport\canDeliver;
    require_once "Transports/Package.php";
    use lab3\Transports\Package;

    class Train extends Transport implements canDeliver
    { 
        public function deliver(Package $gruz)
        {
            if ($this->maxWeight <= $gruz->weight){
                return "Груз ".$gruz->name." слишком тяжелый для поезда ". $this->name;
            }
            if ($this->maxVolume <= $gruz->volume){
                return "Груз ".$gruz->name." слишком большой для поезда ". $this->name;
            }
            return "Поезд ".$this->name." "." перевёз груз "." ".$gruz->name;
        }
    }

?>