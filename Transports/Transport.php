<?php
    namespace lab3\Transports;

    class Transport
    { 
        public $name;
        public $maxWeight;
        public $maxVolume;

        function __construct($name, $maxWeight, $maxVolume)
        {
            $this->name = $name;
            $this->maxWeight = $maxWeight;
            $this->maxVolume = $maxVolume;
        }
    }

?>