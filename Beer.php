<?php 

namespace Pivo;
    class Beer 
    {
        public $name;
        public $fortress;
        public $colour;
        public $fltered;
        

        function __construct($name, $fortress, $colour, $filtered)
        {
            $this->name = $name;
            $this->fortress = $fortress;
            $this->colour = $colour;
            $this->filtered = $filtered;
        }

        function displayInfo()
        {
            echo "Пиво ".$this->name." крепостью ".$this->fortress." ".$this->colour." ".$this->filtered."<br>";
        }
    }

    class Ale extends Beer //класс Эль
    {
        function type() {
            echo "Тип: Эль <br>";
        }
    }

    class Lager extends Beer //класс Лагер
    {
        function type() {
            echo "Тип: Лагер <br>";
        }
    }

    class German_lager extends Lager 
    {
        function type() {
            echo "Тип: Немецкий Лагер <br>";
        }
    }

    class American_lager extends Lager 
    {
        function type() {
            echo "Тип: Американский Лагер <br>";
        }
    }

    class Porter extends Lager 
    {
        function type() {
            echo "Тип: Портер <br>";
        }
    }

    class Lambic extends Ale
    {
        function type() {
            echo "Тип: Ламбик <br>";
        }
    }

    class Irish_ale extends Ale
    {
        function type() {
            echo "Тип: Ирландский эль <br>";
        }
    }

    class German_ale extends Ale
    {
        function type() {
            echo "Тип: Немецкий Эль <br>";
        }
    }

    
?>