<?php 
    //Транспорт
    echo "Транспорт<br>";

    require_once "Transports/Package.php";
    require_once "Transports/Transport.php";
    require_once "Transports/Car.php";
    require_once "Transports/Ship.php";
    require_once "Transports/Train.php";
    use lab3\Transports\Transport;
    use lab3\Transports\Package;
    use lab3\Transports\Car;
    use lab3\Transports\Ship;
    use lab3\Transports\Train;


    $car = new Car("Валдай: NEXT", 4000, 50);
    $ship = new Ship("Контейнеровоз", 8000000, 135000);
    $train = new Train("Томас", 1020000, 1680);

    $coal = new Package('Уголь', 2500, 120);
    $bag = new Package('Сумка', 20, 15);
    $meat = new Package('Мясо', 250000, 2000);

    echo $car->deliver($meat);
    echo "<br>";
    echo $ship->deliver($meat);
    echo "<br>";
    echo $train->deliver($meat);
    echo "<br>";
    echo "<br>";
    echo $car->deliver($bag);
    echo "<br>";
    echo $ship->deliver($bag);
    echo "<br>";
    echo $train->deliver($bag);
    echo "<br>";
    echo "<br>";
    echo $car->deliver($coal);
    echo "<br>";
    echo $ship->deliver($coal);
    echo "<br>";
    echo $train->deliver($coal);
    echo "<br>";
    echo "<br>";

    //Дерево
    use Pivo\Ale;
    use Pivo\American_lager;
    use Pivo\German_ale;
    use Pivo\German_lager;
    use Pivo\Irish_ale;
    use Pivo\Lambic;
    require_once "Beer.php";

    echo "Дерево<br>";
    echo "<br>";
    $fruct_beer = new Lambic("Фруктовое пиво", 5, "Светлое", "Фильтрованное");
    $Wiseberg = new German_ale("Вайсберг", 5.5, "Светлое", "Фильтрованное");
    $Guinness = new Irish_ale("Гиннес", 3.8, "Тёмное", "Фильтрованное");
    $Corona = new American_lager("Корона", 4.5, "Светлое", "Фильтрованное");
    $Spaten = new German_lager("Шпатен", 5.2, "Светлое", "Фильтрованное");
    $MohSmel = new Ale("Мохнатый шмель", 5, "Тёмное", "Нефильтрованный");

    $fruct_beer->displayInfo();
    $fruct_beer->type();
    $Wiseberg->displayInfo();
    $Wiseberg->type();
    $Guinness->displayInfo();
    $Guinness->type();
    $Corona->displayInfo();
    $Corona->type();
    $Spaten->displayInfo();
    $Spaten->type();
    $MohSmel->displayInfo();
    $MohSmel->type();
    echo "<br>";
    


    //Фастфуд
    echo "Фастфуд<br>";

    require_once "Fastfood/Food.php";
    use lab3\Fastfood\Food;
    require_once "Fastfood/Customer.php";
    use lab3\Fastfood\Customer;
    require_once "Fastfood/Cashier.php";
    use lab3\Fastfood\Cashier;
    require_once "Fastfood/Cook.php";
    use lab3\Fastfood\Cook;

    $burger = new Food("Чизбургер", 59);
    $fri = new Food("Картофель Фри", 59);
    $cofe = new Food("Американо", 55);

    $customer = new Customer("Иван");
    $cook1 = new Cook("Антон", false);
    $cook2 = new Cook("Максим", true);
    $CookList = [$cook1, $cook2];
    $cashier = new Cashier("Петр", $CookList);

    $order = [$burger, $fri, $cofe];

    $customer->giveOrder($order, 200, $cashier)

    
?>