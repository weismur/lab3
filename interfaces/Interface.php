<?php
    namespace interfaces\transport;
    require_once "Transports/Package.php";
    use lab3\Transports\Package;
    
    interface canDeliver
        {
            function deliver(Package $gruz) ;
        }

?>